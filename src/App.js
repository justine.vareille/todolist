import React, { Component } from 'react';
import './App.css';
import List from './components/List';
import AddTodo from './components/AddTodo';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() { 
    return ( 
      <div>
        <AddTodo />
        <List />
      </div>
     );
  }
}
 
export default App;
