import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, } from 'reactstrap';
import {addTodo} from '../redux/actions/actions';
import {connect} from 'react-redux';

class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      newTodo : ''
     }
  }

handleChange = (event) => {
  this.setState({newTodo: event.target.value})
}

handleSubmit = (event) => {
  event.preventDefault();
  this.props.submit(this.state.newTodo)
}

  render() { 
    return ( 
      <div>
        <Form>
          <FormGroup>
            <Label for="exampleText">Add a Todo</Label>
            <Input onChange={this.handleChange} type="textarea" name="text" id="exampleText" />
          </FormGroup>
          <Button onClick={this.handleSubmit}>Submit</Button>
        </Form>
      </div>
     );
  }
}

const mapDispatchToProps = (dispatch) =>{
  return {
    submit : (todo) =>{
      dispatch(addTodo(todo))
    }
  }
}
 
export default connect(null, mapDispatchToProps)(AddTodo);