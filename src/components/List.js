import React, { Component } from 'react';
import {connect} from 'react-redux';

const mapStateToProps = (state) => ({todos : state.todos})

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() { 
    const {todos} = this.props;
    return ( 
      <div>
        {todos.map((todo)=>(
          <div>
            <p>{todo}</p>
          </div>
        ))}
      </div>
     );
  }
}
 
export default connect(mapStateToProps)(List);