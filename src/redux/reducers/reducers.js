const initialState = {
  todos: [
      'contenu 1',
      'contenu 2',
  ],
};

function rootReducer(state = initialState, action) {
  switch(action.type){
    case 'ADD_TODO': {
      return{
      ...state,
      todos:[...state.todos, action.newTodo]
      }
    }
    default : { 
      return state;
    }
  }
}

export default rootReducer;