import {ADD_TODO} from '../constants/action-type';

export const addTodo = (newTodo) => {
  return {
    type: ADD_TODO,
    newTodo
  }
}

